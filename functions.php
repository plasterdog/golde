<?php
/**
 * plasterdog functions and definitions
 *
 * @package plasterdog
 */

/**
 * Set the content width based on the theme's design and stylesheet.
 */
if ( ! isset( $content_width ) ) {
	$content_width = 640; /* pixels */
}

if ( ! function_exists( 'plasterdog_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function plasterdog_setup() {

	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on plasterdog, use a find and replace
	 * to change 'plasterdog' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'plasterdog', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	//add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => __( 'Primary Menu', 'plasterdog' ),
	) );



	// Setup the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'plasterdog_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );

	// Enable support for HTML5 markup.
	add_theme_support( 'html5', array(
		'comment-list',
		'search-form',
		'comment-form',
		'gallery',
	) );
}
endif; // plasterdog_setup
add_action( 'after_setup_theme', 'plasterdog_setup' );



/**
 * Register widgetized area and update sidebar with default widgets.
 */
function plasterdog_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Archive & Post Sidebar', 'plasterdog' ),
		'id'            => 'sidebar-1',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h1 class="widget-title">',
		'after_title'   => '</h1>'
	) );

        register_sidebar( array(
        'name'          => __( 'Regular Page Sidebar', 'plasterdog' ),
        'id'            => 'sidebar-3',
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget'  => '</aside>',
        'before_title'  => '<h1 class="widget-title">',
        'after_title'   => '</h1>'
    ) );

    register_sidebar( array(
        'name'          => __( 'Feed Page Sidebar', 'plasterdog' ),
        'id'            => 'sidebar-2',
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget'  => '</aside>',
        'before_title'  => '<h1 class="widget-title">',
        'after_title'   => '</h1>'
    ) );
        register_sidebar( array(
        'name'          => __( 'Question Archive Sidebar', 'plasterdog' ),
        'id'            => 'sidebar-4',
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget'  => '</aside>',
        'before_title'  => '<h1 class="widget-title">',
        'after_title'   => '</h1>'
    ) );
}
add_action( 'widgets_init', 'plasterdog_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function plasterdog_scripts() {
	wp_enqueue_style( 'plasterdog-style', get_stylesheet_uri() );

	wp_enqueue_script( 'plasterdog-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20120206', true );

	wp_enqueue_script( 'plasterdog-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20130115', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'plasterdog_scripts' );

/**
 * Implement the Custom Header feature. - JMC ACTIVATED
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';



//JMC => https://code.tutsplus.com/tutorials/a-guide-to-the-wordpress-theme-customizer-adding-a-new-setting--wp-33180
//CUSTOMIZER COLOR ADDITIONS
//SETTING UP THE NEW COLOR CONTROL: LINKS
function pdog1_register_theme_customizer( $wp_customize ) {
    $wp_customize->add_setting(
        'pdog_link_color',
        array(
            'default'     => '#00649c'
        )
    );
    $wp_customize->add_control(
        new WP_Customize_Color_Control(
            $wp_customize,
            'link_color',
            array(
                'label'      => __( 'Link Color', 'tcx' ),
                'section'    => 'colors',
                'settings'   => 'pdog_link_color'
            )
        )
    );
}
add_action( 'customize_register', 'pdog1_register_theme_customizer' );
//JMC THIS APPLIES THE COLOR ABOVE AS A CSS OVER-RIDE
function pdog1_customizer_css() {
    ?>
    <style type="text/css">
       a { color: <?php echo get_theme_mod( 'pdog_link_color' ); ?>; }
    </style>
    <?php
}
add_action( 'wp_head', 'pdog1_customizer_css' );
//SETTING UP THE NEW COLOR CONTROL: LINKS:HOVER
function pdog2_register_theme_customizer( $wp_customize ) {
    $wp_customize->add_setting(
        'pdog_link_hover',
        array(
            'default'     => '#001f30'
        )
    );
    $wp_customize->add_control(
        new WP_Customize_Color_Control(
            $wp_customize,
            'link_hover',
            array(
                'label'      => __( 'Link Hover', 'tcx' ),
                'section'    => 'colors',
                'settings'   => 'pdog_link_hover'
            )
        )
    );
}
add_action( 'customize_register', 'pdog2_register_theme_customizer' );
//JMC THIS APPLIES THE COLOR ABOVE AS A CSS OVER-RIDE
function pdog2_customizer_css() {
    ?>
    <style type="text/css">
       a:hover { color: <?php echo get_theme_mod( 'pdog_link_hover' ); ?>; }
    </style>
    <?php
}
add_action( 'wp_head', 'pdog2_customizer_css' );
//SETTING UP THE NEW COLOR CONTROL: SOCIAL MEDIA LINKS
function pdog3_register_theme_customizer( $wp_customize ) {
    $wp_customize->add_setting(
        'pdog_social_color',
        array(
            'default'     => '#000000'
        )
    );
    $wp_customize->add_control(
        new WP_Customize_Color_Control(
            $wp_customize,
            'social_color',
            array(
                'label'      => __( 'Social Media Link Color', 'tcx' ),
                'section'    => 'colors',
                'settings'   => 'pdog_social_color'
            )
        )
    );
}
add_action( 'customize_register', 'pdog3_register_theme_customizer' );


//JMC THIS APPLIES THE COLOR ABOVE AS A CSS OVER-RIDE
function pdog3_customizer_css() {
    ?>
    <style type="text/css">
      #right-head .social a,  #right-head .social{ color: <?php echo get_theme_mod( 'pdog_social_color' ); ?>; }
    </style>
    <?php
}
add_action( 'wp_head', 'pdog3_customizer_css' );

//SETTING UP THE NEW COLOR CONTROL: SOCIAL MEDAI LINKS:HOVER
function pdog4_register_theme_customizer( $wp_customize ) {
    $wp_customize->add_setting(
        'pdog_social_hover',
        array(
            'default'     => '#001f30'
        )
    );
    $wp_customize->add_control(
        new WP_Customize_Color_Control(
            $wp_customize,
            'social_hover',
            array(
                'label'      => __( 'Social Media Link Hover', 'tcx' ),
                'section'    => 'colors',
                'settings'   => 'pdog_social_hover'
            )
        )
    );
}
add_action( 'customize_register', 'pdog4_register_theme_customizer' );
//JMC THIS APPLIES THE COLOR ABOVE AS A CSS OVER-RIDE
function pdog4_customizer_css() {
    ?>
    <style type="text/css">
       #right-head .social a:hover { color: <?php echo get_theme_mod( 'pdog_link_hover' ); ?>; }
    </style>
    <?php
}
add_action( 'wp_head', 'pdog4_customizer_css' );
//SETTING UP THE NEW COLOR CONTROL: HEADINGS COLOR
function pdog5_register_theme_customizer( $wp_customize ) {
    $wp_customize->add_setting(
        'pdog_headings_color',
        array(
            'default'     => '#322215'
        )
    );
    $wp_customize->add_control(
        new WP_Customize_Color_Control(
            $wp_customize,
            'headings_color',
            array(
                'label'      => __( 'Headings Color', 'tcx' ),
                'section'    => 'colors',
                'settings'   => 'pdog_headings_color'
            )
        )
    );
}
add_action( 'customize_register', 'pdog5_register_theme_customizer' );
//JMC THIS APPLIES THE COLOR ABOVE AS A CSS OVER-RIDE
function pdog5_customizer_css() {
    ?>
    <style type="text/css">
      h1, h2, h3, h4, h5, h6, h1.page-title, h1.entry-title,h1.page-title a, h1.entry-title a, h1.responsive-page-title, h1.responsive-page-title a, h1.answer-heading, .single-question h1.page-title{ color: <?php echo get_theme_mod( 'pdog_headings_color' ); ?>; }
      button, input[type="button"], input[type="reset"], input[type="submit"]{ background-color: <?php echo get_theme_mod( 'pdog_headings_color' ); ?>; }
    </style>
    <?php
}
add_action( 'wp_head', 'pdog5_customizer_css' );
//SETTING UP THE NEW COLOR CONTROL: BODY TEXT
function pdog6_register_theme_customizer( $wp_customize ) {
    $wp_customize->add_setting(
        'pdog_body_text',
        array(
            'default'     => '#000000'
        )
    );
    $wp_customize->add_control(
        new WP_Customize_Color_Control(
            $wp_customize,
            'body_text',
            array(
                'label'      => __( 'Body Text Color', 'tcx' ),
                'section'    => 'colors',
                'settings'   => 'pdog_body_text'
            )
        )
    );
}
add_action( 'customize_register', 'pdog6_register_theme_customizer' );
//JMC THIS APPLIES THE COLOR ABOVE AS A CSS OVER-RIDE
function pdog6_customizer_css() {
    ?>
    <style type="text/css">
       body { color: <?php echo get_theme_mod( 'pdog_body_text' ); ?>; }
    </style>
    <?php
}
add_action( 'wp_head', 'pdog6_customizer_css' );
//SETTING UP THE NEW COLOR CONTROL: CONTENT BACKGROUND COLOR
function pdog7_register_theme_customizer( $wp_customize ) {
    $wp_customize->add_setting(
        'pdog_content_background',
        array(
            'default'     => '#ffffff'
        )
    );
    $wp_customize->add_control(
        new WP_Customize_Color_Control(
            $wp_customize,
            'content_background',
            array(
                'label'      => __( 'Content Area Background', 'tcx' ),
                'section'    => 'colors',
                'settings'   => 'pdog_content_background'
            )
        )
    );
}
add_action( 'customize_register', 'pdog7_register_theme_customizer' );
//JMC THIS APPLIES THE COLOR ABOVE AS A CSS OVER-RIDE
function pdog7_customizer_css() {
    ?>
    <style type="text/css">
      #content { background: <?php echo get_theme_mod( 'pdog_content_background' ); ?>; }
    </style>
    <?php
}
add_action( 'wp_head', 'pdog7_customizer_css' );
//SETTING UP THE NEW COLOR CONTROL: HEADER BACKGROUND COLOR
function pdog8_register_theme_customizer( $wp_customize ) {
    $wp_customize->add_setting(
        'pdog_header_background',
        array(
            'default'     => '#cccccc'
        )
    );
    $wp_customize->add_control(
        new WP_Customize_Color_Control(
            $wp_customize,
            'header_background',
            array(
                'label'      => __( 'Header Area Background', 'tcx' ),
                'section'    => 'colors',
                'settings'   => 'pdog_header_background'
            )
        )
    );
}
add_action( 'customize_register', 'pdog8_register_theme_customizer' );
//JMC THIS APPLIES THE COLOR ABOVE AS A CSS OVER-RIDE
function pdog8_customizer_css() {
    ?>
    <style type="text/css">
      #full-top { background: <?php echo get_theme_mod( 'pdog_header_background' ); ?>; }
    </style>
    <?php
}
add_action( 'wp_head', 'pdog8_customizer_css' );

//SETTING UP THE NEW COLOR CONTROL: FOOTER BACKGROUND COLOR
function pdog9_register_theme_customizer( $wp_customize ) {
    $wp_customize->add_setting(
        'pdog_foot_background',
        array(
            'default'     => '#cccccc'
        )
    );
    $wp_customize->add_control(
        new WP_Customize_Color_Control(
            $wp_customize,
            'foot_background',
            array(
                'label'      => __( 'Footer Area Background', 'tcx' ),
                'section'    => 'colors',
                'settings'   => 'pdog_foot_background'
            )
        )
    );
}
add_action( 'customize_register', 'pdog9_register_theme_customizer' );
//JMC THIS APPLIES THE COLOR ABOVE AS A CSS OVER-RIDE
function pdog9_customizer_css() {
    ?>
    <style type="text/css">
      #colophon.site-footer { background: <?php echo get_theme_mod( 'pdog_foot_background' ); ?>; }
    </style>
    <?php
}
add_action( 'wp_head', 'pdog9_customizer_css' );

//SETTING UP THE NEW COLOR CONTROL: FOOTER TEXT COLOR
function pdog10_register_theme_customizer( $wp_customize ) {
    $wp_customize->add_setting(
        'pdog_foot_text',
        array(
            'default'     => '#000000'
        )
    );
    $wp_customize->add_control(
        new WP_Customize_Color_Control(
            $wp_customize,
            'foot_text',
            array(
                'label'      => __( 'Footer Text Color', 'tcx' ),
                'section'    => 'colors',
                'settings'   => 'pdog_foot_text'
            )
        )
    );
}
add_action( 'customize_register', 'pdog10_register_theme_customizer' );
//JMC THIS APPLIES THE COLOR ABOVE AS A CSS OVER-RIDE
function pdog10_customizer_css() {
    ?>
    <style type="text/css">
      .footer-section {color: <?php echo get_theme_mod( 'pdog_foot_text' ); ?>; }
    </style>
    <?php
}
add_action( 'wp_head', 'pdog10_customizer_css' );






// JMC- unregister widgets selectively
 function unregister_default_widgets() {
     unregister_widget('WP_Widget_Calendar');
     unregister_widget('WP_Widget_Categories');
     unregister_widget('WP_Widget_Meta');
	 unregister_widget('WP_Widget_Archives');
     unregister_widget('WP_Widget_Recent_Posts');
	 unregister_widget('WP_Widget_Recent_Comments');
     unregister_widget('WP_Widget_RSS');
     unregister_widget('WP_Widget_Tag_Cloud');
 }
 add_action('widgets_init', 'unregister_default_widgets', 11);

 /* JMC --* Enable support for Post Thumbnails	 */
	add_theme_support( 'post-thumbnails' );



// JMC-Allows extra HTML items in to the_excerpt instead of stripping them like WordPress does
function theme_t_wp_improved_trim_excerpt($text) {
    global $post;
    if ( '' == $text ) {
        $text = get_the_content('');
        $text = apply_filters('the_content', $text);
        $text = str_replace(']]>', ']]&gt;', $text);
        $text = preg_replace('@<script[^>]*?>.*?</script>@si', '', $text);
        $text = strip_tags($text, '<p>,<ul>,<li>,<ol>');
        $excerpt_length = 55;
        $words = explode(' ', $text, $excerpt_length + 1);
        if (count($words)> $excerpt_length) {
            array_pop($words);
            array_push($words, '[...]');
            $text = implode(' ', $words);
        }
    }
    return $text;
}
 
remove_filter('get_the_excerpt', 'wp_trim_excerpt');
add_filter('get_the_excerpt', 'theme_t_wp_improved_trim_excerpt');
/* JMC- remove html filters from category descriptions*/
$filters = array( 'pre_term_description' );

foreach ( $filters as $filter ) {
    remove_filter( $filter, 'wp_filter_kses' );
}

foreach ( array( 'term_description' ) as $filter ) {
    remove_filter( $filter, 'wp_kses_data' );
}




// JMC - allows shortcodes in widgets
add_filter('widget_text', 'do_shortcode');
/**--- JMC OBSCURES LOGIN FAILURE MESSAGE---*/
     add_filter('login_errors',create_function('$a', "return null;"));

// JMC reset header image size from the default of 1000x 250
define( 'HEADER_IMAGE_WIDTH', apply_filters( 'twentyeleven_header_image_width', 150 ) );
define( 'HEADER_IMAGE_HEIGHT', apply_filters( 'twentyeleven_header_image_height', 100 ) );

//JMC remove anchor link from more tag

function remove_more_anchor($link) {
     $offset = strpos($link, '#more-');
     if ($offset) {
          $end = strpos($link, '"',$offset);
     }
     if ($end) {
          $link = substr_replace($link, '', $offset, $end-$offset);
     }
     return $link;
}
add_filter('the_content_more_link', 'remove_more_anchor');

//* -JMC-Replace WordPress login logo with your own
add_action('login_head', 'b3m_custom_login_logo');
function b3m_custom_login_logo() {
echo '<style type="text/css">
h1 a { background-image:url('.get_stylesheet_directory_uri().'/images/login.png) !important; background-size: 209px 150px !important;height: 150px !important; width: 209px !important; margin-bottom: 0 !important; padding-bottom: 0 !important; }
.login form { margin-top: 10px !important; }
</style>';
}
// JMC- custom footer message
function modify_footer_admin () {
  echo 'Themed and configured by <a href="http://plasterdog.com">plasterdog web design</a>. ';
  echo 'CMS Powered by<a href="http://WordPress.org"> WordPress !</a>';
}
add_filter('admin_footer_text', 'modify_footer_admin');

// JMC Remove WordPress Widgets from Dashboard Area
function remove_wp_dashboard_widgets(){

    remove_meta_box('dashboard_incoming_links', 'dashboard', 'normal');  // Incoming Links
    remove_meta_box('dashboard_quick_press', 'dashboard', 'side');  // Quick Press
    remove_meta_box('dashboard_recent_drafts', 'dashboard', 'side');  // Recent Drafts
    remove_meta_box('dashboard_primary', 'dashboard', 'side');   // WordPress blog (News)
    
}
add_action('wp_dashboard_setup', 'remove_wp_dashboard_widgets');

//Remove  WordPress Welcome Panel
remove_action('welcome_panel', 'wp_welcome_panel');
// JMC - change the standard wordpress greeting
add_action( 'admin_bar_menu', 'wp_admin_bar_my_custom_account_menu', 11 );

function wp_admin_bar_my_custom_account_menu( $wp_admin_bar ) {
$user_id = get_current_user_id();
$current_user = wp_get_current_user();
$profile_url = get_edit_profile_url( $user_id );

if ( 0 != $user_id ) {
/* Add the "My Account" menu */
$avatar = get_avatar( $user_id, 28 );
$howdy = sprintf( __('Start editing, %1$s'), $current_user->display_name );
$class = empty( $avatar ) ? '' : 'with-avatar';

$wp_admin_bar->add_menu( array(
'id' => 'my-account',
'parent' => 'top-secondary',
'title' => $howdy . $avatar,
'href' => $profile_url,
'meta' => array(
'class' => $class,
),
) );

}
}

// JMC - COMBINING BOTH CUSTOM WIDGETS INTO A SINGLE WIDGET
add_action('wp_dashboard_setup', 'my_dashboard_widgets');

function my_dashboard_widgets() {
     global $wp_meta_boxes;
     unset(
          $wp_meta_boxes['dashboard']['normal']['core']['dashboard_plugins'],
          $wp_meta_boxes['dashboard']['side']['core']['dashboard_secondary'],
          $wp_meta_boxes['dashboard']['side']['core']['dashboard_primary']
     );

add_meta_box( 'dashboard_custom_feed', 'Welcome to your customized site!', 'dashboard_custom_feed_output', 'dashboard', 'side', 'high' );
}
function dashboard_custom_feed_output() {
     echo '<div class="rss-widget">';
     echo '<p>Your site has been significantly customized and many functions exist only in your theme, so think twice before changing it!</p>
 <p>Have a question? contact Jeff McNear by email: <a href="mailto:jeff@plasterdog.com">here</a>. </p>
<p>Old school? give me a call at: 847/849-7060</p>
<p>For a list of tutorials <a href="http://plasterdog.com/category/wordpress-tutorials/" target="_blank">Follow this link</a>
<p><strong>Here are some recent tutorials:</strong></p><hr/>';
     wp_widget_rss_output(array(
          'url' => 'http://plasterdog.com/category/wordpress-tutorials/feed/',
          'title' => 'MY_FEED_TITLE',
          'items' => 5,
          'show_summary' => 0,
          'show_author' => 0,
          'show_date' => 0
     ));

     echo '</div>';
}

?>
