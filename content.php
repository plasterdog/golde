<?php
/**
 * @package plasterdog
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<h1 class="entry-title"><a href="<?php the_permalink(); ?>" rel="bookmark">
<?php the_post_thumbnail( 'thumbnail' ); ?>
			<?php the_title(); ?></a></h1>

		<?php if ( 'post' == get_post_type() ) : ?>
		<div class="entry-meta">
			
		</div><!-- .entry-meta -->
		<?php endif; ?>
	</header><!-- .entry-header -->

	<?php if ( is_search() ) : // Only display Excerpts for Search ?>
	<div class="entry-summary">
		<?php the_excerpt(); ?>
        <p align="right"><a href="<?php the_permalink(); ?>" rel="bookmark">... read the rest...</a></p>
	</div><!-- .entry-summary -->
	<?php else : ?>
	<div class="entry-content">
<?php the_excerpt(); ?>
<p align="right"><a href="<?php the_permalink(); ?>" rel="bookmark">... read the entire post</a></p>
	</div><!-- .entry-content -->
</article><!-- #post-## -->
<div class="clear"><hr/></div>

	<?php endif; ?>

	<?php if ( ! post_password_required() && ( comments_open() || '0' != get_comments_number() ) ) : ?>
	
	<?php endif; ?>

		
	


