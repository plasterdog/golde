<?php
/**
 * The template for displaying archive pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package amta
 */

get_header(); ?>
<div id="hero-top">
<img src="<?php echo get_stylesheet_directory_uri() ?>/images/default-banner.jpg" alt="<?php bloginfo( 'name' ); ?>" />
</div>  

    <div class="clear"></div>
        <div id="page" class="hfeed site">
  <div id="content" class="site-content" >


	<div id="primary" class="content-area">

   <div class="clear">
			
			<h3><?php single_cat_title( ); ?></h3>
				<div class="archive-description">

					<?php if(get_the_archive_description()) {?>
					<?php	the_archive_description( );	?>
					<?php }   ?>
					<?php if(!get_the_archive_description()) {?>

					<?php }?> <!-- ends the second outer condition -->
					
				</div>
			
<hr/>

  </div><!-- clear -->


	<?php query_posts($query_string . '&orderby=date&order=DESC&showposts=-1'); ?>

<?php if ( have_posts() ) : ?>
			<header class="page-header">
				
			</header><!-- .page-header -->
			<?php
			/* Start the Loop */
			while ( have_posts() ) : the_post(); ?>

<?php if (!empty($post->post_excerpt)) : ?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>  
<div id="secondary" class="widget-area" role="complementary">
	
		<header class="entry-header">
		<h1 class="entry-title"><a href="<?php the_permalink(); ?>" rel="bookmark">Question:</a></h1>	
		</header><!-- .entry-header -->
		<?php the_field('question_body'); ?>

			<?php the_field('question_body'); ?>
            <?php 
            $link = get_field('related_content');
            if( $link ): ?><hr/>
            Related Article:<a class="button" href="<?php echo $link['url']; ?>" target="<?php echo $link['target']; ?>"> <?php echo $link['title']; ?></a>
            <?php endif; ?>

</div>
<main id="main" class="question-site-main" role="main">
<div class="entry-content">
	 <h1 class="answer-heading">Answer: </h1>
    <?php the_excerpt(); ?>		
    <p align="right" style="margin-bottom:.5em;"><a href="<?php the_permalink(); ?>" rel="bookmark">... read the rest</a></p>
	</div><!-- .entry-content -->

<?php else : ?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>  
<div id="secondary" class="widget-area" role="complementary">
	
		<header class="entry-header">
		<h1 class="entry-title">Question:</h1>	
		</header><!-- .entry-header -->
		<?php the_field('question_body'); ?>

            <?php 
            $link = get_field('related_content');
            if( $link ): ?><hr/>
            Related Article:<a class="button" href="<?php echo $link['url']; ?>" target="<?php echo $link['target']; ?>"> <?php echo $link['title']; ?></a>
            <?php endif; ?>

		</div>
<main id="main" class="question-site-main" role="main">
<div class="entry-content">
	 <h1 class="answer-heading">Answer: </h1>
    <?php the_content(); ?>		
	</div><!-- .entry-content -->
 <?php endif; ?>	

</article><!-- #post-## -->		

<div class="clear"><hr/></div>

			
			<?php 
			endwhile;

			the_posts_navigation();

		else :

			get_template_part( 'template-parts/content', 'none' );

		endif; ?>

		</main><!-- #main -->
	</div><!-- #primary -->



<?php get_footer();
