<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package plasterdog
 */

get_header(); ?>
<?php if( get_field('hero_check_off') == 'show' ): ?>

<div id="hero-top">
<?php if ( get_field( 'page_hero_image' ) ): ?>		
<img src="<?php echo esc_url( get_field( 'page_hero_image' ) ); ?>"/>	
<?php else : ?>
<img src="<?php echo get_stylesheet_directory_uri() ?>/images/page-default-banner.jpg" alt="<?php bloginfo( 'name' ); ?>" />
<?php endif; ?>	
</div>

<?php endif; ?>


<?php if( get_field('hero_check_off') == 'hide' ): ?>
<div id="hero-top">

</div>
<?php endif; ?>	
		<div class="clear"></div>

		<div id="page" class="hfeed site">
	<div id="content" class="site-content" >
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php while ( have_posts() ) : the_post(); ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
	<h1 class="responsive-page-title"><?php the_title(); ?></h1>	
	</header><!-- .entry-header -->

	<div class="entry-content">
		<?php the_content(); ?>
		<?php
			wp_link_pages( array(
				'before' => '<div class="page-links">' . __( 'Pages:', 'plasterdog' ),
				'after'  => '</div>',
			) );
		?>
	</div><!-- .entry-content -->
	<?php edit_post_link( __( 'Edit', 'plasterdog' ), '<footer class="entry-footer"><span class="edit-link">', '</span></footer>' ); ?>
</article><!-- #post-## -->



			<?php endwhile; // end of the loop. ?>

		</main><!-- #main -->
	</div><!-- #primary -->

	<div id="secondary" class="widget-area front-book-array" role="complementary">
<!-- THE CONDITIONAL TITLE -->
		<?php if(get_field('sidebar_title')) {?>
			<h1 class="page-title"><a href="<?php the_field('sidebar_link'); ?>"><?php the_field('sidebar_title'); ?></a></h1>
			<hr/>
		<?php } ?><!-- ends the first condition -->
		<?php if(!get_field('sidebar_title')) {?>	
		<h1 class="page-title"><?php the_title(); ?></h1>	
		<?php }?> <!-- ends the second outer condition -->
<!--THE CONDITIONAL SIDEBAR CONTENTS-->
		<?php if(get_field('alternate_sidebar_content')) {?>
			<?php the_field('alternate_sidebar_content'); ?>
		<?php } ?><!-- ends the first condition -->
		<?php if(!get_field('alternate_sidebar_content')) {?>
			<?php if ( ! dynamic_sidebar( 'sidebar-3' ) ) : ?>
			<?php endif; // end sidebar widget area ?>
		<?php }?> <!-- ends the second outer condition -->
	</div><!-- #secondary -->
<?php get_footer(); ?>
