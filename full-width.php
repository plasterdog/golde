<?php
/*
*Template Name: Full Width
 * @package plasterdog
 */

get_header(); ?>
<?php if ( get_field( 'page_hero_image' ) ): ?>	
<div id="hero-top">
<img src="<?php echo esc_url( get_field( 'page_hero_image' ) ); ?>"/>	
</div>
<?php else : ?>
<div id="hero-top">
<img src="<?php echo get_stylesheet_directory_uri() ?>/images/default-banner.jpg" alt="<?php bloginfo( 'name' ); ?>" />
</div>	
<?php endif; ?>
		<div class="clear"></div>

		<div id="page" class="hfeed site">
	<div id="content" class="site-content" >
	<div id="primary" class="full-content-area">
		<main id="main" class="full-site-main" role="main">

			<?php while ( have_posts() ) : the_post(); ?>

				<?php get_template_part( 'content', 'page' ); ?>



			<?php endwhile; // end of the loop. ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_footer(); ?>
