<?php
/**
 * Template Name: Split Page
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package plasterdog
 */

get_header(); ?>

			
<?php if( get_field('hero_check_off') == 'show' ): ?>

<div id="hero-top">
<?php if ( get_field( 'page_hero_image' ) ): ?>		
<img src="<?php echo esc_url( get_field( 'page_hero_image' ) ); ?>"/>	
<?php else : ?>
<img src="<?php echo get_stylesheet_directory_uri() ?>/images/page-default-banner.jpg" alt="<?php bloginfo( 'name' ); ?>" />
<?php endif; ?>	
</div>

<?php endif; ?>


<?php if( get_field('hero_check_off') == 'hide' ): ?>
<div id="hero-top">

</div>
<?php endif; ?>			
	<div class="clear"></div>

		<div id="page" class="hfeed site">
	<div id="content" class="site-content" >
	<div id="primary" class="full-content-area">
		<main id="main" class="full-site-main" role="main">

			<?php while ( have_posts() ) : the_post(); ?>

			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<header class="entry-header">
				
			</header><!-- .entry-header -->

			<div class="entry-content">
				<div class="left-side">
				<?php the_content(); ?>
				<?php
					wp_link_pages( array(
						'before' => '<div class="page-links">' . __( 'Pages:', 'plasterdog' ),
						'after'  => '</div>',
					) );
				?>
			</div><!-- ends left side -->
			<div class="right-side">
				<?php the_field('right_section'); ?>
				
			</div><!-- ends right side-->
			</div><!-- .entry-content -->
	

	<?php edit_post_link( __( 'Edit', 'plasterdog' ), '<footer class="entry-footer"><span class="edit-link">', '</span></footer>' ); ?>
</article><!-- #post-## -->



			<?php endwhile; // end of the loop. ?>

		</main><!-- #main -->
	</div><!-- #primary -->


<?php get_footer(); ?>
