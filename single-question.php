<?php
/**
 * The Template for displaying all single posts.
 *
 * @package plasterdog
 */

get_header(); ?>
<div id="hero-top">
<img src="<?php echo get_stylesheet_directory_uri() ?>/images/default-banner.jpg" alt="<?php bloginfo( 'name' ); ?>" />
</div>  

    <div class="clear"></div>

    <div id="page" class="hfeed site">
  <div id="content" class="site-content" >

  <div id="secondary" class="widget-area" role="complementary">
  <header class="page-header">  <h1 class="page-title"><?php the_title(); ?></h1></header>

            <?php the_field('question_body'); ?>
            <?php 
            $link = get_field('related_content');
            if( $link ): ?><hr/>
            Related Article:<a class="button" href="<?php echo $link['url']; ?>" target="<?php echo $link['target']; ?>"> <?php echo $link['title']; ?></a>
            <?php endif; ?>

  </div><!-- #secondary -->

  <div id="primary" class="content-area">


    <main id="main" class="question-site-main" role="main">

    <?php while ( have_posts() ) : the_post(); ?>

  <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
  <header class="entry-header">
  <div class="entry-meta"></div><!-- .entry-meta -->
  </header><!-- .entry-header -->



  <div class="entry-content">
    <h1 class="answer-heading">Answer: </h1>
    <?php the_content(); ?>




  </div><!-- .entry-content -->

  <footer class="entry-footer">
    

    <?php edit_post_link( __( 'Edit', 'plasterdog' ), '<span class="edit-link">', '</span>' ); ?>
  </footer><!-- .entry-footer -->
</article><!-- #post-## -->

        <div class="clear"><!-- variation from default nav which restricts navigation within category -->
<div class="left-split-nav"><?php //previous_post_link('%link', '&larr; %title', TRUE) ?></div>
<div class="right-split-nav"><?php //next_post_link('%link', '%title &rarr;', TRUE) ?></div>
</div>

      <?php
        // If comments are open or we have at least one comment, load up the comment template
        if ( comments_open() || '0' != get_comments_number() ) :
          comments_template();
        endif;
      ?>

    <?php endwhile; // end of the loop. ?>

    </main><!-- #main -->
  </div><!-- #primary -->



<?php get_footer(); ?>