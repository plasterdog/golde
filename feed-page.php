<?php
/*
*Template Name: Feed Page
 * @package plasterdog
 */

get_header(); ?>
<?php if( get_field('hero_check_off') == 'show' ): ?>

<div id="hero-top">
<?php if ( get_field( 'page_hero_image' ) ): ?>		
<img src="<?php echo esc_url( get_field( 'page_hero_image' ) ); ?>"/>	
<?php else : ?>
<img src="<?php echo get_stylesheet_directory_uri() ?>/images/page-default-banner.jpg" alt="<?php bloginfo( 'name' ); ?>" />
<?php endif; ?>	
</div>
<?php endif; ?>
<?php if( get_field('hero_check_off') == 'hide' ): ?>
<div id="hero-top"></div>
<?php endif; ?>	
		<div class="clear"></div>

		<div id="page" class="hfeed site">
	<div id="content" class="site-content" >
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php while ( have_posts() ) : the_post(); ?>

			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
<?php if(get_field('sidebar_title')) {?>
	<h1 class="responsive-page-title"><a href="<?php the_field('sidebar_link'); ?>"><?php the_field('sidebar_title'); ?></a></h1>
	
<?php } ?><!-- ends the first condition -->
<?php if(!get_field('sidebar_title')) {?>	
<h1 class="responsive-page-title"><?php the_title(); ?></h1>
		
<?php }?> <!-- ends the second outer condition -->
	</header><!-- .entry-header -->

	<div class="entry-content">
		<?php the_content(); ?>
		<?php
			wp_link_pages( array(
				'before' => '<div class="page-links">' . __( 'Pages:', 'plasterdog' ),
				'after'  => '</div>',
			) );
		?>


<?php if ( get_field( 'category_slug_name' ) ): ?>
	
					<?php
					global $post;
					$args = array( 'numberposts' =>get_field('number_of_excerpts'), 'offset'=> 0, 'category_name' =>get_field('category_slug_name'), 'orderby' => 'post_date', 'order' => 'DSC');
					$myposts = get_posts( $args );
					foreach( $myposts as $post ) :	setup_postdata($post); ?>
							
			<?php if ( get_the_post_thumbnail( $post_id ) != '' ) { ?>
			

				<?php if (!empty($post->post_excerpt)) : ?>
			
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>  
			<div class="archive_left_picture">	
			<a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_post_thumbnail( 'medium' ); ?></a>
			</div><!-- ends left picture -->
				<div class="archive_right_text">
				<header class="entry-header">
				<h1 class="entry-title"><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a></h1>	
				</header><!-- .entry-header -->	
				<?php the_excerpt(); ?>
				<p align="right" style="margin-bottom:.5em;"><a href="<?php the_permalink(); ?>" rel="bookmark">find out more</a></p>
				<?php else : ?>
			
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>  
			<div class="archive_left_picture">	
			<?php the_post_thumbnail( 'medium' ); ?>
			</div><!-- ends left picture -->
				<div class="archive_right_text">
				<header class="entry-header">
				<h1 class="entry-title"><?php the_title(); ?></h1>	
				</header><!-- .entry-header -->
				<?php the_content(); ?>
				<?php endif; ?>	
				</div><!-- ends right text -->
			</article><!-- #post-## -->
		    <div class="clear"><hr/></div>

			<?php   } else { ?>
			
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>  


				<?php if (!empty($post->post_excerpt)) : ?>
		<header class="entry-header">
		<h1 class="entry-title"><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a></h1>	
		</header><!-- .entry-header -->		
				<?php the_excerpt(); ?>
				<p align="right" style="margin-bottom:.5em;"><a href="<?php the_permalink(); ?>" rel="bookmark">find out more</a></p>
				<?php else : ?>
		<header class="entry-header">
		<h1 class="entry-title"><?php the_title(); ?></h1>	
		</header><!-- .entry-header -->			
				<?php the_content(); ?>
				<?php endif; ?>	
			</article><!-- #post-## -->	
			<div class="clear"><hr/></div>
		 	<?php    } ?>

			<?php endforeach; ?>

<?php else: // field_name returned false ?>	


<!--if no entry in field query will show posts unfiltered by category -->	

			<?php if ( get_field( 'number_of_excerpts' ) ): ?>
			<?php 
			// the query
			$wpb_all_query = new WP_Query(array('post_type'=>'post', 'post_status'=>'publish', 'posts_per_page' =>get_field('number_of_excerpts'))); ?>

			<?php else: // field_name returned false ?>	
			<?php 
			// the query
			$wpb_all_query = new WP_Query(array('post_type'=>'post', 'post_status'=>'publish', 'posts_per_page'=>-1)); ?>
			<?php endif; // end of if field_name logic ?>	
			
				<!-- the loop -->
			<?php while ( $wpb_all_query->have_posts() ) : $wpb_all_query->the_post(); ?>
					
				<?php if ( get_the_post_thumbnail( $post_id ) != '' ) { ?>
				
				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>  
				<div class="archive_left_picture">	
				<a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_post_thumbnail( 'medium' ); ?></a>
				</div><!-- ends left picture -->
					<div class="archive_right_text">
					<header class="entry-header">
					<h1 class="entry-title"><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a></h1>	
					</header><!-- .entry-header -->
				<?php if (!empty($post->post_excerpt)) : ?>
				<?php the_excerpt(); ?>
				<p align="right" style="margin-bottom:.5em;"><a href="<?php the_permalink(); ?>" rel="bookmark">find out more</a></p>
				<?php else : ?>
				<?php the_content(); ?>
				<?php endif; ?>	
					</div><!-- ends right text -->
				</article><!-- #post-## -->
			    <div class="clear"><hr/></div>

				<?php   } else { ?>
				
				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>  
					<header class="entry-header">
					<h1 class="entry-title"><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a></h1>	
					</header><!-- .entry-header -->
				<?php if (!empty($post->post_excerpt)) : ?>
				<?php the_excerpt(); ?>
				<p align="right" style="margin-bottom:.5em;"><a href="<?php the_permalink(); ?>" rel="bookmark">find out more</a></p>
				<?php else : ?>
				<?php the_content(); ?>
				<?php endif; ?>	
				</article><!-- #post-## -->
				<div class="clear"><hr/></div>		
			 	<?php    } ?>

				<?php endwhile; ?>
				<!-- end of the loop -->

<?php endif; // end of if field_name logic ?>	
<?php wp_reset_postdata(); ?>
</div><!-- .entry-content -->
	<?php edit_post_link( __( 'Edit', 'plasterdog' ), '<footer class="entry-footer"><span class="edit-link">', '</span></footer>' ); ?>
	<?php endwhile; // end of the loop. ?>
	</main><!-- #main -->
	</div><!-- #primary -->

	<div id="secondary" class="widget-area front-book-array" role="complementary">
<!-- THE CONDITIONAL TITLE -->
		<?php if(get_field('sidebar_title')) {?>
			<h1 class="page-title"><a href="<?php the_field('sidebar_link'); ?>"><?php the_field('sidebar_title'); ?></a></h1>
			<hr/>
		<?php } ?><!-- ends the first condition -->
		<?php if(!get_field('sidebar_title')) {?>	
		<h1 class="page-title"><?php the_title(); ?></h1>	
		<?php }?> <!-- ends the second outer condition -->
<!--THE CONDITIONAL SIDEBAR CONTENTS-->
		<?php if(get_field('alternate_sidebar_content')) {?>
			<?php the_field('alternate_sidebar_content'); ?>
		<?php } ?><!-- ends the first condition -->
		<?php if(!get_field('alternate_sidebar_content')) {?>
			<?php if ( ! dynamic_sidebar( 'sidebar-2' ) ) : ?>
			<?php endif; // end sidebar widget area ?>
		<?php }?> <!-- ends the second outer condition -->
	</div><!-- #secondary -->
<?php get_footer(); ?>
